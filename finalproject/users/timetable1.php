
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<html lang="en">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Time Table </title>
    
    <!-- Custom CSS -->
<style>
  td{
      text-align:center;
  }
  .form-control{
      width:50%;
  }
</style>
    
    <![endif]-->

</head>
    <body>

<form method="POST">
<div class="row" style="margin-bottom: 10px;">
<?php echo @$err; ?>
<script>
function showSemester(str)
{
if (str=="")
{
document.getElementById("txtHint").innerHTML="";
return;
}

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("semester").innerHTML=xmlhttp.responseText;
}
}
//alert(str);
xmlhttp.open("GET","semester_ajax.php?id="+str,true);
xmlhttp.send();
}
</script>
<div class="row" style="margin-bottom: 10px;"> 
	<select name="courseid" class="form-control" onchange="showSemester(this.value)" id="courseid"/>
    <option disabled selected value="null">Select Department</option>
	<?php 
	$dep=mysqli_query($con,"select * from department");
	while($dp=mysqli_fetch_array($dep))
	{
	$dp_id=$dp[0];
	echo "<option value='$dp_id'>".$dp[1]."</option>";
	}
	?>
    </select>
	</div>
   
    <div class="row" style="margin-bottom: 10px;">
	<select name="s" id="semester"  class="form-control"/>
    <option disabled selected value="null" >Select Semester</option>
    <?php
	$sub=mysqli_query($con,"select * from semester");
	while($s=mysqli_fetch_array($sub))
	{
		$s_id=$s[0];
		echo "<option value='$s_id'>".$s[1]."</option>";
	}
	?>
	</select>
	</div>
    <br>
	<br>
 <div class="row" style="margin-bottom: 10px;">
	<input type="submit" value="Submit" name="save" class="btn btn-success" />
	<input type="reset" value="Reset" name="reset" onclick="removetable()" class="btn btn-success"/>
</div>

</form>
<script>
    function removetable(){
        var element = document.getElementById("table");
        element.parentNode.removeChild(element);
    }
</script>
<?php
include('../config.php');
extract($_POST);
if(isset($save)){
    if($s!='null' || $s=='' && $courseid!='null'){
        $que=mysqli_query($con,"select * from subject where sem_id='$s' and department_id='$courseid'");
        if($que==null){
            echo "nothing";
        }
        $sub=array();
        $i=0;
        $lab=array();
        $j=0;
        while($res=mysqli_fetch_array($que))
        {
            if(strpos($res['subject_name'],"LAB")==true){
                $lab[$j++]=$res['subject_name'];
            }
            else{
                $sub[$i++]=$res['subject_name'];
            }
        }
        if(count($sub)!=0){
            if($s%5==0){
                echo "<table border='1' id='table' align='center' class='danger'>";
                echo "<Tr><th style='text-align:center'>Days</th><th style='text-align:center'>Hour 1</th><th style='text-align:center'>Hour 2</th><th style='text-align:center'>Hour 3</th><th style='text-align:center'>Hour 4</th><th style='text-align:center'>Hour 5</th><th style='text-align:center'>Hour 6</th><th style='text-align:center'>Hour 7</th><th style='text-align:center'>Hour 8</th></tr>";
                echo "<Tr><th style='text-align:center'>Mon</th><td style='text-align:center'>$sub[3]</td><td style='text-align:center'>$sub[2]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[1]</td><td style='text-align:center' colspan='4'>$lab[1]</td></tr>";
                echo "<Tr><th style='text-align:center'>Tues</th><td style='text-align:center'>$sub[0]</td><td style='text-align:center'>$sub[1]</td><td style='text-align:center'>$sub[2]</td><td style='text-align:center'>$sub[3]</td><td style='text-align:center' colspan='4'>$lab[0]</td></tr>";
                echo "<Tr><th style='text-align:center'>Wed</th><td style='text-align:center'>$sub[1]</td><td style='text-align:center'>$sub[0]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[3]</td><td colspan='4' style='text-align:center'>$lab[2]</td></tr>";
                echo "<Tr><th style='text-align:center'>Thurs</th><td colspan='2' style='text-align:center'>$lab[1]</td><td style='text-align:center'>$sub[2]</td><td style='text-align:center'>$sub[3]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[1]</td><td>$sub[3]</td><td >Free Hour</td></tr>";
                echo "<Tr><th style='text-align:center'>Fri</th><td colspan='2' style='text-align:center'>$lab[2]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[0]</td><td style='text-align:center' colspan='2'>$lab[0]</td><td style='text-align:center'>$sub[1]</td><td >Free Hour</td></tr>";
                echo "</table>";
                }else if($s%5==1){
                echo "<table border='1' id='table' align='center' class='danger'>";
                echo "<Tr><th style='text-align:center'>Days</th><th style='text-align:center'>Hour 1</th><th style='text-align:center'>Hour 2</th><th style='text-align:center'>Hour 3</th><th style='text-align:center'>Hour 4</th><th style='text-align:center'>Hour 5</th><th style='text-align:center'>Hour 6</th><th style='text-align:center'>Hour 7</th><th style='text-align:center'>Hour 8</th></tr>";
                echo "<Tr><th style='text-align:center'>Mon</th><td style='text-align:center' colspan='4'>$lab[1]</td><td style='text-align:center'>$sub[3]</td><td style='text-align:center'>$sub[2]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[1]</td></tr>";
                echo "<Tr><th style='text-align:center'>Tues</th><td colspan='2' style='text-align:center'>$lab[1]</td><td style='text-align:center'>$sub[2]</td><td style='text-align:center'>$sub[3]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[1]</td><td>$sub[3]</td><td colspan='1' style='text-align:center'>Free Hour</td></tr>";              
                echo "<Tr><th style='text-align:center'>Wed</th><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[0]</td><td colspan='2' style='text-align:center'>$lab[0]</td><td style='text-align:center'>$sub[1]</td><td style='text-align:center'>Free Hour</td><td colspan='2' style='text-align:center'>$lab[2]</td></tr>";
                echo "<Tr><th style='text-align:center'>Thurs</th><td style='text-align:center'>$sub[0]</td><td style='text-align:center'>$sub[1]</td><td style='text-align:center'>$sub[2]</td><td style='text-align:center'>$sub[3]</td><td style='text-align:center' colspan='4'>$lab[0]</td></tr>";
                echo "<Tr><th style='text-align:center'>Fri</th><td style='text-align:center'>$sub[1]</td><td style='text-align:center'>$sub[0]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[3]</td><td colspan='4' style='text-align:center'>$lab[2]</td></tr>";
                echo "</table>";
                }else if($s%5==2){
                echo "<table border='1' id='table' align='center' class='danger'>";
                echo "<Tr><th style='text-align:center'>Days</th><th style='text-align:center'>Hour 1</th><th style='text-align:center'>Hour 2</th><th style='text-align:center'>Hour 3</th><th style='text-align:center'>Hour 4</th><th style='text-align:center'>Hour 5</th><th style='text-align:center'>Hour 6</th><th style='text-align:center'>Hour 7</th><th style='text-align:center'>Hour 8</th></tr>";
                echo "<Tr><th style='text-align:center'>Mon</th><td style='text-align:center'>$sub[0]</td><td style='text-align:center'>$sub[1]</td><td style='text-align:center'>$sub[2]</td><td style='text-align:center'>$sub[3]</td><td style='text-align:center' colspan='4'>$lab[0]</td></tr>";
                echo "<Tr><th style='text-align:center'>Tues</th><td style='text-align:center'>$sub[3]</td><td style='text-align:center'>$sub[2]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[1]</td><td style='text-align:center' colspan='4'>$lab[1]</td></tr>";
                echo "<Tr><th style='text-align:center'>Wed</th><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[0]</td><td colspan='2' style='text-align:center'>$lab[0]</td><td style='text-align:center'>$sub[1]</td><td style='text-align:center'>$sub[3]</td><td colspan='2' style='text-align:center'>$lab[2]</td></tr>";
                echo "<Tr><th style='text-align:center'>Thurs</th><td colspan='2' style='text-align:center'>$lab[1]</td><td style='text-align:center'>$sub[2]</td><td style='text-align:center'>$sub[3]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[1]</td><td colspan='2' style='text-align:center'>Free Hours</td></tr>";
                echo "<Tr><th style='text-align:center'>Fri</th><td style='text-align:center'>$sub[1]</td><td style='text-align:center'>$sub[0]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[3]</td><td colspan='4' style='text-align:center'>$lab[2]</td></tr>";
                echo "</table>";
                }
                else if($s%5==3){
                echo "<table border='1' id='table' align='center' class='danger'>";
                echo "<Tr><th style='text-align:center'>Days</th><th style='text-align:center'>Hour 1</th><th style='text-align:center'>Hour 2</th><th style='text-align:center'>Hour 3</th><th style='text-align:center'>Hour 4</th><th style='text-align:center'>Hour 5</th><th style='text-align:center'>Hour 6</th><th style='text-align:center'>Hour 7</th><th style='text-align:center'>Hour 8</th></tr>";
                echo "<Tr><th style='text-align:center'>Mon</th><td style='text-align:center'>$sub[0]</td><td style='text-align:center'>$sub[1]</td><td style='text-align:center'>$sub[2]</td><td style='text-align:center'>$sub[3]</td><td style='text-align:center' colspan='4'>$lab[0]</td></tr>";
                echo "<Tr><th style='text-align:center'>Tues</th><td style='text-align:center'>$sub[3]</td><td style='text-align:center'>$sub[2]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[1]</td><td style='text-align:center' colspan='4'>$lab[1]</td></tr>";
                echo "<Tr><th style='text-align:center'>Wed</th><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[0]</td><td colspan='2' style='text-align:center'>$lab[0]</td><td style='text-align:center'>$sub[1]</td><td style='text-align:center'>$sub[3]</td><td colspan='2' style='text-align:center'>$lab[2]</td></tr>";
                echo "<Tr><th style='text-align:center'>Thurs</th><td colspan='2' style='text-align:center'>$lab[1]</td><td style='text-align:center'>$sub[2]</td><td style='text-align:center'>$sub[3]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[1]</td><td colspan='2' style='text-align:center'>Free Hours</td></tr>";
                echo "<Tr><th style='text-align:center'>Fri</th><td style='text-align:center'>$sub[1]</td><td style='text-align:center'>$sub[0]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[3]</td><td colspan='4' style='text-align:center'>$lab[2]</td></tr>";
                echo "</table>";
                }else {
                    echo "<table border='1' id='table' align='center' class='danger'>";
                    echo "<Tr><th style='text-align:center'>Days</th><th style='text-align:center'>Hour 1</th><th style='text-align:center'>Hour 2</th><th style='text-align:center'>Hour 3</th><th style='text-align:center'>Hour 4</th><th style='text-align:center'>Hour 5</th><th style='text-align:center'>Hour 6</th><th style='text-align:center'>Hour 7</th><th style='text-align:center'>Hour 8</th></tr>";
                    echo "<Tr><th style='text-align:center'>Mon</th><td style='text-align:center'>$sub[1]</td><td style='text-align:center'>$sub[0]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[3]</td><td colspan='4' style='text-align:center'>$lab[2]</td></tr>";
                    echo "<Tr><th style='text-align:center'>Tues</th><td style='text-align:center'>$sub[0]</td><td style='text-align:center'>$sub[1]</td><td style='text-align:center'>$sub[2]</td><td style='text-align:center'>$sub[3]</td><td style='text-align:center' colspan='4'>$lab[0]</td></tr>";
                    echo "<Tr><th style='text-align:center'>Wed</th><td style='text-align:center'>$sub[3]</td><td style='text-align:center'>$sub[2]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[1]</td><td style='text-align:center' colspan='4'>$lab[1]</td></tr>";
                    echo "<Tr><th style='text-align:center'>Thurs</th><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[0]</td><td colspan='2' style='text-align:center'>$lab[0]</td><td style='text-align:center'>$sub[1]</td><td style='text-align:center'>$sub[3]</td><td colspan='2' style='text-align:center'>$lab[2]</td></tr>";
                    echo "<Tr><th style='text-align:center'>Fri</th><td colspan='2' style='text-align:center'>$lab[1]</td><td style='text-align:center'>$sub[2]</td><td style='text-align:center'>$sub[3]</td><td style='text-align:center'>$sub[4]</td><td style='text-align:center'>$sub[1]</td><td colspan='2' style='text-align:center'>Free Hours</td></tr>";
                    echo "</table>";
                }
        }
    }
}

?>
</body>
